FROM alpine:edge

ARG UID=1000
ARG GID=1000

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
#RUN apk update && apk upgrade

RUN apk add --no-cache \
    pandoc \
    graphviz \
    chromium \
    nss \
    freetype \
    freetype-dev \
    harfbuzz \
    ca-certificates \
    ttf-freefont \
    font-fira-code \
    nodejs \
    yarn \
    make \
    plantuml \
    py3-pip \
    qpdf

# commands from 

# install LaTeX
# Parts of this file are copied from https://github.com/pandoc/dockerfiles
RUN apk --no-cache add \
    freetype \
    fontconfig \
    gnupg \
    gzip \
    librsvg \
    perl \
    tar \
    wget \
    xz
# DANGER: this will vary for different distributions, particularly the
# `linuxmusl` suffix.  Alpine linux is a musl libc based distribution, for other
# "more common" distributions, you likely want just `-linux` suffix rather than
# `-linuxmusl` ------------------------> vvvvvvvvv
ENV PATH="/opt/texlive/texdir/bin/x86_64-linuxmusl:${PATH}"
WORKDIR /root
COPY pandoc-docker/latex/texlive.profile /root/texlive.profile
COPY pandoc-docker/latex/install-texlive.sh /root/install-texlive.sh
RUN echo "binary_x86_64-linuxmusl 1" >> /root/texlive.profile
RUN /root/install-texlive.sh
COPY pandoc-docker/latex/install-tex-packages.sh /root/install-tex-packages.sh
RUN /root/install-tex-packages.sh
RUN rm -f /root/texlive.profile \
    /root/install-texlive.sh \
    /root/install-tex-packages.sh
WORKDIR /data

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser \
    PATH="/data/node_modules/.bin:${PATH}"

ENV PATH="/usr/local/texlive/bin/x86_64-linuxmusl:${PATH}"

# Add user so we don't need --no-sandbox.
RUN addgroup -S pptruser -g $GID && adduser -S -G pptruser -u $UID pptruser \
    && mkdir -p /home/pptruser \
    && chown -R pptruser:pptruser /data \  
    && chmod o+w /opt/texlive/texdir/texmf-var \
    && chown -R pptruser:pptruser /home/pptruser

# Puppeteer v5.2.1 works with Chromium 85.
RUN yarn add eslint puppeteer@5.2.1 mermaid-filter

RUN cat /etc/apk/repositories

# Install Tex Gyre Termes font
RUN tlmgr update --self && tlmgr install tex-gyre tex-gyre-math selnolig

# Install Noto Color Emoji
RUN mkdir -p /usr/share/fonts/truetype/noto \
    && cd /usr/share/fonts/truetype/noto \
    && wget https://raw.githubusercontent.com/googlefonts/noto-emoji/master/fonts/NotoColorEmoji.ttf \
    && fc-cache -fv

RUN pip install pandoc-plantuml-filter
RUN pip install pandoc-include

# Allow pptruser to get root via sudo
RUN apk add --no-cache alpine-sdk sudo \
    && echo "pptruser ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Run everything after as non-privileged user.
USER pptruser
WORKDIR /home/pptruser
COPY src/puppeteerConfigFile.json /home/pptruser/.puppeteer.json
